package com.example.clientservice.controller;

import com.example.clientservice.feign.BookServiceFeignClient;
import com.example.clientservice.model.Book;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/client")
public class ClientRestController {
    private final BookServiceFeignClient client;

    @GetMapping("/books")
    List<Book> getAllBooksFromClient() {
        return client.getAllBooks();
    }
}
